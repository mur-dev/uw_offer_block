# UW Offer Blocks Module
This module shows program and faculty specific content to the user based on the URL parameters. The users are prospective students who have been given an offer to a program.

[uwaterloo.ca/my-offer/](https://uwaterloo.ca/my-offer/)

---
---
# Functions

---
---
### function uw_offer_blocks_node_view($node, $view_mode = 'full', $langcode = NULL)
This hook gives you the ability to act on and modify a node that is being assembled before it is rendered. Additionally, this function retrieves the program and faculty code from the URL. The codes represent the actual program and faculty name. For example, "ARTS1H" represents the program "Honours Arts", and "AR" represents the "Arts" faculty. It retrieves the content for the program and the faculty and displays it through node manipulation. In order to modify a node, you would implement this:

##### Sample code
$node->content['my_field'] = array(

&nbsp;&nbsp;&nbsp;&nbsp;'#markup' => $my_markup

);

##### Parameters
- $node: The node that is being assembled for rendering.
- $view_mode: The $view_mode parameter from node_view().
- $langcode: The language code used for rendering.

##### Additional Information
See the [api documentation](https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_node_view/7.x) for this specific hook for more information.

---
---
# Functions from api.inc

---
---
### function uw_offer_blocks_program_faculty_check($program, $faculty)
This function determines if the program and the faculty code provided exists. Additionally, it checks to see if the program is paired with the correct faculty.

##### Parameters
- $program: The program code that is retrieved from the URL.
- $faculty: The faculty code that is retrieved from the URL.

##### Returns
The taxonomy name of the program and the faculty. For example, if $program was "ARTS1H" and the $faculty was "AR", it would return "Honours Arts" and "Arts".

---
---
### function uw_offer_blocks_term_display($content, $term_type)
This function loads the program and faculty content.

##### Parameters
- $content: The taxonomy name. For example, "Honours Arts".
- $term_type: The name of the taxonomy we want to retrieve from. For exmaple, "program", because we want to retrieve "Honours Arts" from the "program" taxonomy list.

##### Returns
The content for the program or faculty depending on the input parameters.