<?php

/**
 * @file
 * Set of methods used for the my offer module.
 */

/**
 * Determines whether a program is part of the correct faculty or not
 */
function uw_offer_blocks_program_faculty_check($program, $faculty) {

  $program_code_mapping = array(
    'AH' => array(
      'UW-AHSBASE1ND' => 'BASE for Applied Health Sciences (1)',
      'UW-AHSBASE2ND' => 'BASE for Applied Health Sciences (2)',
      'UW-HLTHSCIH' => 'Health Studies',
      'UW-HLTHSCIHC' => 'Health Studies (Co-op)',
      'UW-KINH' => 'Kinesiology',
      'UW-KINHC' => 'Kinesiology (Co-op)',
      'UW-PUBHLTHH' => 'Public Health',
      'UW-PUBHLTHHC' => 'Public Health (Co-op)',
      'UW-RECH' => 'Recreation and Leisure Studies',
      'UW-RECHC' => 'Recreation and Leisure Studies (Co-op)',
    ),
    'AR' => array(
      'UW-ARTBASE1ND' => 'BASE for Arts (1)',
      'UW-ARTBASE2ND' => 'BASE for Arts (2)',
      'UW-ARTBUSH' => 'Honours Arts and Business',
      'UW-ARTBUSHC' => 'Honours Arts and Business (Co-op)',
      'UW-ARTS1H' => 'Honours Arts',
      'UW-ARTS1HC' => 'Honours Arts (co-op)',
      'UW-ARTS3G' => 'Liberal Studies (3-year)',
      'UW-ARTS4G' => 'Liberal Studies (4-year)',
      'UW-GBDAHC' => 'Global Business and Digital Arts (Co-op)',
      'REN-ARTBUSH' => 'Honours Arts and Business (Renison)',
      'REN-ARTBUSHC' => 'Honours Arts and Business (Co-op, Renison)',
      'REN-ARTS1H' => 'Honours Arts (Renison)',
      'REN-ARTS1HC' => 'Honours Arts (co-op, Renison)',
      'REN-SDEV16H' => 'Social Development Studies',
      'REN-SDEV3G' => 'Social Development Studies (3-year)',
      'REN-SDEV4G' => 'Social Development Studies (4-year)',
      'STJ-ARTBUSH' => 'Honours Arts and Business (St. Jerome\'s)',
      'STJ-ARTBUSHC' => 'Honours Arts and Business (Co-op, St. Jerome\'s)',
      'STJ-ARTS1H' => 'Honours Arts (St. Jerome\'s)',
      'STJ-ARTS1HC' => 'Honours Arts (co-op, St. Jerome\'s)',
	    'ONLN-ARTS1H' => 'Online Honours Arts',
      'ONLNR-SDEV16H' => 'Online Honours Social Development Studies’ (Renison)',
    ),
    'EN' => array(
      'UW-AE' => 'Architectural Engineering',
      'UW-ARCHPPENG' => 'Architectural Studies',
      'UW-BIOMEDE' => 'Biomedical Engineering',
      'UW-CHE' => 'Chemical Engineering',
      'UW-CIVE' => 'Civil Engineering',
      'UW-COMPE' => 'Computer Engineering',
      'UW-ELE' => 'Electrical Engineering',
      'UW-ENGBASE2ND' => 'BASE for Engineering',
      'UW-ENVE' => 'Environmental Engineering',
      'UW-GEOE' => 'Geological Engineering',
      'UW-ME' => 'Mechanical Engineering',
      'UW-MECTR' => 'Mechatronics Engineering',
      'UW-MGTE' => 'Management Engineering',
      'UW-NE' => 'Nanotechnology Engineering',
      'UW-SYDE' => 'Systems Design Engineering',
    ),
    'ES' => array(
      'UW-CECH' => 'Climate and Environmental Change',
      'UW-CECHC' => 'Climate and Environmental Change (co-op)',
      'UW-ENVBASE1ND' => 'BASE for Environment (1)',
      'UW-ENVBASE2ND' => 'BASE for Environment (2)',
      'UW-ENVBUSHC' => 'Environment and Business (Co-op)',
      'UW-ERSH' => 'Environment, Resources and Sustainability',
      'UW-ERSHC' => 'Environment, Resources and Sustainability (Co-op)',
      'UW-GEOGAVTNH' => 'Geography and Aviation',
      'UW-GEOGEMH' => 'Geography and Environmental Management',
      'UW-GEOGEMHC' => 'Geography and Environmental Management (Co-op)',
      'UW-GEOMATH' => 'Geomatics',
      'UW-GEOMATHC' => 'Geomatics (Co-op)',
      'UW-INTDEVH' => 'International Development',
      'UW-KIH' => 'Knowledge Integration',
      'UW-PLANHC' => 'Planning',
    ),
    'MA' => array(
      'UW-BUSCSHC' => 'Business Administration and Computer Science Double Degree',
      'UW-BUSMATHC' => 'Business Administration and Mathematics Double Degree',
      'UW-CSBH' => 'Computer Science',
      'UW-CSBHC' => 'Computer Science (Co-op)',
      'UW-MACACRHC' => 'Mathematics/Chartered Professional Accountancy',
      'UW-MATBASE1ND' => 'Math/BASE',
      'UW-MATHBAH' => 'Mathematics/Business Administration',
      'UW-MATHBAHC' => 'Mathematics/Business Administration (Co-op)',
      'UW-MATHFARMH' => 'Mathematics/Financial Analysis and Risk Management',
      'UW-MATHFARMHC' => 'Mathematics/Financial Analysis and Risk Management (Co-op)',
      'UW-MATHH' => 'Mathematics',
      'UW-MATHHC' => 'Mathematics (Co-op)',
      'ONLN-MATHFARMHC' => 'Online Mathematics/Financial Analysis and Risk Management (co-op)',
	    'ONLN-MATHFARMH' => 'Online Mathematics/Financial Analysis and Risk Management',
    ),
    'SC' => array(
      'UW-ENVSCIH' => 'Environmental Science',
      'UW-ENVSCIHC' => 'Environmental Science (Co-op)',
      'UW-LIFESCIH' => 'Life Sciences',
      'UW-LIFESCIHC' => 'Life Sciences (Co-op)',
      'UW-PHYSCIH' => 'Physical Sciences',
      'UW-PHYSCIHC' => 'Physical Sciences (Co-op)',
      'UW-SCBTCACRHC' => 'Biotechnology/Chartered Professional Accountancy',
      'UW-SCBTECHC' => 'Biotechnology/Economics',
      'UW-SCIAVTNH' => 'Science and Aviation',
      'UW-SCIBASE1ND' => 'BASE for Science (1)',
      'UW-SCIBASE2ND' => 'BASE for Science (2)',
      'UW-SCIBUSH' => 'Science and Business',
      'UW-SCIBUSHC' => 'Science and Business (Co-op)',
      'UW-SCIH' => 'Honours Science',
    ),
    'ID' => array(
      'UW-AFMPHC' => 'Accounting and Financial Management',
      'UW-CFMHC' => 'Computing and Financial Management',
      'UW-SE' => 'Software Engineering',
      'UW-SFMH' => 'Sustainability and Financial Management',
      'UW-SFMHC' => 'Sustainability and Financial Management (co-op)',
    ),
  );

  $faculty_code_mapping = array(
    'AH' => 'Applied Health Sciences',
    'AR' => 'Arts',
    'ES' => 'Environment',
    'EN' => 'Engineering',
    'MA' => 'Math',
    'SC' => 'Science',
    'ID' => 'ID',
  );

  if (array_key_exists($faculty, $program_code_mapping) && array_key_exists($program, $program_code_mapping[$faculty])) {

  $taxonomy[] = $program_code_mapping[$faculty][$program];

  $exception_array = [
    'UW-KIH',
    'REN-ARTS1H',
    'REN-ARTS1HC',
    'REN-ARTBUSH',
    'REN-ARTBUSHC',
    'REN-SDEV16H',
    'REN-SDEV3G',
    'REN-SDEV4G',
    'ONLNR-SDEV16H',
    'STJ-ARTBUSH',
    'STJ-ARTBUSHC',
    'STJ-ARTS1H',
    'STJ-ARTS1HC',
  ];

  if(in_array($program, $exception_array)) {
    $faculty = "ID";
  }

  $taxonomy[] = $faculty_code_mapping[$faculty];

  return $taxonomy;
  }
}

/**
 * Load HTML by ($content) taxonomy name, ($term_type) the machine name of BLOCKS inside the vocabulary
 */
function uw_offer_blocks_term_display($content, $term_type) {
  $content_array = array();
  $output = array();
  $machine_name = 'field_' . $term_type . '_blocks';

  foreach ($content as $paragraph) {
    if (isset($paragraph->$machine_name)) {
      foreach ($paragraph->$machine_name as $item) {
        foreach ($item as $element) {
          $content_array += entity_load("paragraphs_item", array($element["value"]));
        }
      }
    }

    //Load the array from the taxonomy
    foreach ($content_array as $entities) {

      switch ($entities->bundle) {

        case 'copy_block':

          $content_html = '<div class="entity entity-paragraphs-item paragraphs-item-copy-block" about="" typeof=""><div class="content uw-section--inner"><div class="field field-name-field-sph-copy field-type-text-long field-label-hidden">' . $entities->field_sph_copy["und"][0]["value"] . '</div></div></div>';
          break;

        case 'sph_marketing_block':

          // Grab the first marketing item in the marketing block
          $marketing_content = entity_load("paragraphs_item", array($entities->field_marketing_items["und"][0]["value"]));
          $entity = array_shift($marketing_content);

          // Load marketing content
          $marketing_image = $entity->field_sph_background_image["und"][0]["filename"];
          $quote_text = $entity->field_marketing_item_content["und"][0]["value"];

          $content_html = '<div class="entity entity-paragraphs-item paragraphs-item-sph-marketing-block" about="" typeof=""><div class="content uw-section--inner"><div class="paragraphs-items paragraphs-items-field-marketing-items paragraphs-items-field-marketing-items-full paragraphs-items-full"><div class="marketing-item-wrap" style="background-image:url(\'/my-offer/sites/ca.my-offer/files/' . $marketing_image . '\');"><div class="marketing-item">' . $quote_text . ' </div></div></div></div></div>';
          break;

        case 'sph_image_block':

          // Load image content
          $image_source = $entities->field_sph_image_image_block["und"][0]["filename"];
          $image_height = $entities->field_sph_image_image_block["und"][0]["height"];
          $image_width = $entities->field_sph_image_image_block["und"][0]["width"];

          $content_html = '<div class="entity entity-paragraphs-item paragraphs-item-sph-image-block" about="" typeof=""><div class="content"><div class="field field-name-field-sph-image-image-block field-type-image field-label-hidden"><div class="field-items"><img src="/my-offer/sites/ca.my-offer/files/' . $image_source . '" width="' . $image_width . '" height="' . $image_height . '" /></div></div></div></div>';
          break;

        case 'sph_quicklinks_block':

          $quicklinks_title = $entities->field_sph_block_title["und"][0]["value"];

          // Grab the quicklinks content
          $quicklinks_content = entity_load("paragraphs_item", array($entities->field_sph_quicklink["und"][0]["value"]));
          $entity = array_shift($quicklinks_content);

          // Load quicklink images and title
          $image_source = $entity->field_sph_icon_image["und"][0]["filename"];
          $width = $entity->field_sph_icon_image["und"][0]["width"];
          $height = $entity->field_sph_icon_image["und"][0]["height"];
          $section_title = $entity->field_sph_link_section_title["und"][0]["value"];

          // Load links into an array
          foreach ($entity->field_sph_links as $link_content) {
            foreach ($link_content as $link) {
              $link_array[] = '<p class="field field-name-field-sph-links field-type-link-field field-label-hidden"><a href="' . $link["url"] . '">' . $link["title"] . '</a></p>' ;
            }
          }

          $content_html = '<div id="quick-link-menu"><div class ="uw-section--inner"><div id="quick-link-content" class="uw-section--inner"><div class="quick-link-group"><div class="paragraphs-items paragraphs-items-field-sph-quicklink paragraphs-items-field-sph-quicklink-full paragraphs-items-full"><div class="quick-link"><img src="/my-offer/sites/ca.my-offer/files/uploads/images/' . $image_source . '" width="' . $width . '" height="' . $height . '" /><h3>' . $section_title . '</h3>' . implode($link_array) . '</div></div></div></div></div></div>';
          break;

        default:

          break;
      }

      $output[] = $content_html;
    }
  }

  return $output;
}
